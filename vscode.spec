Name:       vscode
Version:    1.68.0
Release:    1%{?dist}
Summary:    Code editing. Redefined.
Group:      Development/Tools
Vendor:     Microsoft Corporation
Packager:   Visual Studio Code Team <vscode-linux@microsoft.com>
License:    MIT
URL:        https://code.visualstudio.com/
ICON:       code.xpm
Source0:    %{name}-%{version}.tar.gz
Source1:    yarn-offline-cache.tar.gz
Source2:    node-v17.4.7-headers.tar.gz
Source3:    extensions.tar.gz
Source4:    ripgrep-v13.0.0-4-x86_64-unknown-linux-musl.tar.gz
Source5:    ripgrep-v13.0.0-4-aarch64-unknown-linux-gnu.tar.gz
Source6:    tocache.tar.gz
Source7:    armtocache.tar.gz
Patch0:     offline.patch
BuildRequires: gcc-c++
BuildRequires: nodejs >= 1:16.14.0
BuildRequires: nodejs < 1:17.0.0
BuildRequires: yarnpkg
BuildRequires: python38
BuildRequires: git
BuildRequires: npm
BuildRequires: fakeroot
BuildRequires: libX11-devel
BuildRequires: libxkbfile-devel
BuildRequires: libsecret-devel
AutoReq:    0

%global __provides_exclude_from ^%{_datadir}/%{name}/.*\\.so.*$

%description
Visual Studio Code is a new choice of tool that combines the simplicity of a code editor with what developers need for the core edit-build-debug cycle. See https://code.visualstudio.com/docs/setup/linux for installation instructions and FAQ.

# Don't generate build_id links to prevent conflicts when installing multiple
# versions of VS Code alongside each other (e.g. `code` and `code-insiders`)

%global debug_package %{nil}
%define _build_id_links none

%prep
%setup -q -n %{name}-%{version}
tar -zxf %{SOURCE1} -C /tmp/
tar -zxf %{SOURCE3} -C /tmp/

if [ ! -d "/tmp/vscode-ripgrep-cache-1.14.2/" ]; then
	mkdir /tmp/vscode-ripgrep-cache-1.14.2/
fi

dir=$(cd ~/ && pwd)
cachedir=$dir'/.cache/'
if [ ! -d $cachedir ]; then
    mkdir $cachedir
fi

%ifarch x86_64
%global arch x64
	cp %{SOURCE4} /tmp/vscode-ripgrep-cache-1.14.2/
	tar -zxf %{SOURCE6} -C $cachedir
%endif

%ifarch aarch64
%global arch arm64
	cp %{SOURCE5} /tmp/vscode-ripgrep-cache-1.14.2/
	tar -zxf %{SOURCE7} -C $cachedir
%endif

%patch0 -p1


%build
node --version

dir=$(cd ~/ && pwd)
gypcachedir=$dir'/.cache/node-gyp/'
if [ -d $gypcachedir ]; then
	rm -rf ~/.cache/node-gyp/*
fi

npm --version
npm config set python python3.8
npm config set tarball  %{SOURCE2}
export PYTHON=$(which python3.8) 

yarn --version
yarn config set yarn-offline-mirror /tmp/yarn-offline-cache/
yarn --offline
yarn --offline run gulp vscode-linux-%{arch}-min
yarn --offline run gulp vscode-linux-%{arch}-build-rpm

%install

cp -r .build/linux/rpm/%{_arch}/rpmbuild/BUILD/usr %{buildroot}


%post
# Remove the legacy bin command if this is the stable build
if [ "%{name}" = "code" ]; then
	rm -f /usr/local/bin/code
fi

# Symlink bin command to /usr/bin
ln -sf /usr/share/code-oss/bin/code-oss %{_bindir}/%{name}

# Register yum repository
# TODO: #229: Enable once the yum repository is signed
#if [ "%{name}" != "code-oss" ]; then
#	if [ -d "/etc/yum.repos.d" ]; then
#		REPO_FILE=/etc/yum.repos.d/%{name}.repo
#		rm -f $REPO_FILE
#		echo -e "[%{name}]\nname=@@NAME_LONG@@\nbaseurl=@@UPDATEURL@@/api/rpm/@@QUALITY@@/@@ARCHITECTURE@@/rpm" > $REPO_FILE
#	fi
#fi

# Update mimetype database to pickup workspace mimetype
update-mime-database /usr/share/mime &> /dev/null || :

%postun
#if [ $1 = 0 ]; then
#  rm -f /usr/bin/%{name}
#fi

# Update mimetype database for removed workspace mimetype
update-mime-database /usr/share/mime &> /dev/null || :

%files
%defattr(-,root,root)
/usr/share/code-oss/
/usr/share/applications/
/usr/share/mime/packages/
/usr/share/pixmaps/
/usr/share/bash-completion/completions/
/usr/share/zsh/site-functions/


%changelog
* Tue Jul  25 2022 aa1hshh <974658390@qq.com>
- 1.68.0
